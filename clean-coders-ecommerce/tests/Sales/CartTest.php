<?php

declare(strict_types=1);

namespace CleanCoders\Tests\Sales;

use CleanCoders\Sales\Cart;
use PHPUnit\Framework\TestCase;
use CleanCoders\Sales\Factory\ItemFactory;
use CleanCoders\Sales\Exception\ProductNotFoundException;
use CleanCoders\Sales\Exception\QuantityDeletionException;
use CleanCoders\Sales\Exception\NotAllowedQuantityException;

final class CartTest extends TestCase
{
    private Cart $cart;

    public function setUp(): void
    {
        parent::setUp();
        $this->cart = new Cart();
    }

    public function test_can_create_a_new_cart(): void
    {
        $this->assertInstanceOf(Cart::class, $this->cart);
    }

    public function test_cart_must_be_unique(): void
    {
        $secondCart = new Cart();
        $this->assertNotSame($this->cart->getId(), $secondCart->getId());
    }

    public function test_a_new_cart_must_be_empty_by_default(): void
    {
        self::assertCount(0, $this->cart->getItems());
    }

    public function test_add_one_product_into_cart(): void
    {
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 1
        ]));

        self::assertCount(1, $this->cart->getItems());

        $item = $this->cart->getItem('ABCDEF');

        self::assertSame('ABCDEF', $item->getProduct()->getSku()); // sku: product identifier
        self::assertSame(1, $item->getOrderedQuantity());
    }

    public function test_add_two_different_products_into_cart(): void
    {
        $items = [
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 10.99,
                ],
                'ordered_quantity' => 1,
            ],
            [
                'product' => [
                    'sku' => 'BCDEFG',
                    'name' => 'shirt',
                    'price' => 20.99,
                ],
                'ordered_quantity' => 3,
            ],
        ];

        foreach ($items as $item) {
            $this->cart->addItem(ItemFactory::create($item));
        }

        self::assertCount(2, $this->cart->getItems());

        $item = $this->cart->getItem('BCDEFG');

        self::assertSame('BCDEFG', $item->getProduct()->getSku());
        self::assertSame(3, $item->getOrderedQuantity());
    }

    public function test_add_one_product_many_times_into_cart(): void
    {
        $items = [
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 10.99,
                ],
                'ordered_quantity' => 1,
            ],
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 20.99,
                ],
                'ordered_quantity' => 3,
            ],
        ];

        foreach ($items as $item) {
            $this->cart->addItem(ItemFactory::create($item));
        }

        self::assertCount(1, $this->cart->getItems());

        $item = $this->cart->getItem('ABCDEF');

        self::assertSame('ABCDEF', $item->getProduct()->getSku());
        self::assertSame(4, $item->getOrderedQuantity());
    }

    public function test_add_one_product_into_cart_with_zero_as_quantity(): void
    {
        $this->expectException(NotAllowedQuantityException::class);
        $this->expectExceptionMessage('You are not allowed to add item with quantity less or equals to zero. [0] given.');
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 0
        ]));
    }

    public function test_add_one_product_into_cart_with_negative_quantity(): void
    {
        $this->expectException(NotAllowedQuantityException::class);
        $this->expectExceptionMessage('You are not allowed to add item with quantity less or equals to zero. [-1] given.');
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => -1
        ]));
    }

    public function test_add_one_product_into_cart_and_remove_this_one(): void
    {
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 1
        ]));

        self::assertCount(1, $this->cart->getItems());

        $this->cart->removeItem('ABCDEF');

        self::assertNull($this->cart->getItem('ABCDEF'));
        self::assertCount(0, $this->cart->getItems());
    }

    public function test_add_one_product_into_cart_and_remove_not_existing(): void
    {
        $this->expectException(ProductNotFoundException::class);
        $this->expectExceptionMessage('Product with sku [unknown] does not exist.');
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 1,
        ]));

        self::assertCount(1, $this->cart->getItems());

        $this->cart->removeItem('unknown');
    }

    public function test_add_one_product_into_cart_and_remove_some_quantity(): void
    {
        // given 10 items of sku 'ABCDEF' were added
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 10
        ]));

        self::assertCount(1, $this->cart->getItems());

        // when I remove 3 items of sku 'ABCDEF'
        $this->cart->removeItem('ABCDEF', 3);

        // then 7 items of sku 'ABCDEF' must remain.
        self::assertCount(1, $this->cart->getItems(), 'empty items');

        $item = $this->cart->getItem('ABCDEF');

        self::assertSame('ABCDEF', $item->getProduct()->getSku());
        self::assertSame(7, $item->getOrderedQuantity());
    }

    public function test_add_one_product_into_cart_and_remove_all_quantity(): void
    {
        // given 10 items of sku 'ABCDEF' were added
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 10
        ]));

        self::assertCount(1, $this->cart->getItems());

        // when I remove all items of sku 'ABCDEF'
        $this->cart->removeItem('ABCDEF', 10);

        // then the item of sku 'ABCDEF' should be gone
        self::assertCount(0, $this->cart->getItems(), 'at least one item is still present.');
        self::assertNull($this->cart->getItem('ABCDEF'), 'item still present.');
    }

    public function test_add_one_product_into_cart_and_remove_more_quantity(): void
    {
        $this->expectException(QuantityDeletionException::class);
        $this->expectExceptionMessage('You are not allowed to remove more quantity. Actual [3], given [4].');

        // given 3 items of sku 'ABCDEF' were added
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 3
        ]));

        self::assertCount(1, $this->cart->getItems());

        // when I remove 4 items of sku 'ABCDEF'
        $this->cart->removeItem('ABCDEF', 4);

        // then an exception must be thrown
    }

    public function test_get_empty_cart_total_price(): void
    {
        self::assertSame(0.0, $this->cart->getTotalPrice(), 'wrong cart total price');
    }

    public function test_add_one_product_and_get_cart_total_price(): void
    {
        $this->cart->addItem(ItemFactory::create([
            'product' => [
                'sku' => 'ABCDEF',
                'name' => 'pant',
                'price' => 10.99,
            ],
            'ordered_quantity' => 3
        ]));

        self::assertSame(10.99 * 3, $this->cart->getTotalPrice(), 'wrong cart total price');
    }

    public function test_add_two_products_and_get_cart_total_price(): void
    {
        $items = [
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 100.99,
                ],
                'ordered_quantity' => 3,
            ],
            [
                'product' => [
                    'sku' => 'BCDEFG',
                    'name' => 'shirt',
                    'price' => 200.99,
                ],
                'ordered_quantity' => 5,
            ],
        ];

        foreach ($items as $item) {
            $this->cart->addItem(ItemFactory::create($item));
        }

        self::assertSame((100.99 * 3) + (200.99 * 5), $this->cart->getTotalPrice(), 'wrong cart total price');
    }

    public function test_add_one_product_many_times_and_get_cart_total_price(): void
    {
        $items = [
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 100.99,
                ],
                'ordered_quantity' => 3,
            ],
            [
                'product' => [
                    'sku' => 'ABCDEF',
                    'name' => 'pant',
                    'price' => 100.99,
                ],
                'ordered_quantity' => 5,
            ],
        ];

        foreach ($items as $item) {
            $this->cart->addItem(ItemFactory::create($item));
        }

        self::assertSame((100.99 * 8), $this->cart->getTotalPrice(), 'wrong cart total price');
    }
}
