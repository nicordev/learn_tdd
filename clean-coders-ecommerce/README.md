# Kata TDD (Test-Driven Development) in PHP (Ecommerce project)

- [live coding](https://www.youtube.com/watch?v=C0wkeNzQqCg)
- [repository](https://github.com/ulrich-geraud/kata-tdd-ecommerce-php)

# setup

```sh
mkdir ecommerce
cd ecommerce
composer init
composer require phpunit/phpunit ^10 --dev
composer req symfony/var-dumper --dev
mkdir src tests
composer dump-autoload
vendor/bin/phpunit # to assert that phpunit works properly
```
