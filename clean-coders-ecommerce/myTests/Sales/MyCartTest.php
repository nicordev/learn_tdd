<?php

declare(strict_types=1);

namespace My\Tests\Sales;

use CleanCoders\Sales\Cart;
use PHPUnit\Framework\TestCase;

final class MyCartTest extends TestCase
{
    private Cart $cart;

    public function setUp(): void
    {
        parent::setUp();
        $this->cart = new Cart();
    }

    public function test_cart_getId_must_be_idempotent(): void
    {
        $this->assertSame($this->cart->getId(), $this->cart->getId());
    }

    public function test_get_unknown_item_must_return_null(): void
    {
        self::assertNull($this->cart->getItem('unknownSku'));
    }
}
