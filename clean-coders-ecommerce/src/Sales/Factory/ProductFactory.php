<?php

declare(strict_types=1);

namespace CleanCoders\Sales\Factory;

use CleanCoders\Sales\Product;

abstract class ProductFactory
{
    /**
     * @param array<string, string|float> $productData
     */
    public static function create(array $productData): Product
    {
        return new Product(
            sku: $productData['sku'],
            name: $productData['name'],
            price: $productData['price'],
        );
    }
}

/*
*/