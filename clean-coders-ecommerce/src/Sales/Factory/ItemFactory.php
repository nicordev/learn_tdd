<?php

declare(strict_types=1);

namespace CleanCoders\Sales\Factory;

use CleanCoders\Sales\Item;

abstract class ItemFactory
{
    /**
     * @param array<string, int|array<string, string|float>> $itemData
     */
    public static function create(array $itemData): Item
    {
        return new Item(
            ProductFactory::create($itemData['product']),
            $itemData['ordered_quantity'],
        );
    }
}
