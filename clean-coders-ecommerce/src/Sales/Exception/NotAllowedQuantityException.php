<?php

declare(strict_types=1);

namespace CleanCoders\Sales\Exception;

use RuntimeException;

final class NotAllowedQuantityException extends RuntimeException
{
    public function __construct(int $value)
    {
        parent::__construct(
            \sprintf(
                'You are not allowed to add item with quantity less or equals to zero. [%d] given.',
                $value,
            )
        );
    }
}