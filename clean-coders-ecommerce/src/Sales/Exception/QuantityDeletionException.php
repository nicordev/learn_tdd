<?php

declare(strict_types=1);

namespace CleanCoders\Sales\Exception;

use RuntimeException;

final class QuantityDeletionException extends RuntimeException
{
    public function __construct(int $currentOrderedQuantity, int $quantityToRemove)
    {
        parent::__construct(
            \sprintf(
                'You are not allowed to remove more quantity. Actual [%d], given [%d].',
                $currentOrderedQuantity,
                $quantityToRemove,
            )
        );
    }
}
