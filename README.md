# learn TDD

some resources about TDD

## [Clean coders](https://cleancoders.fr/)

- [Live coding kata Test-Driven Development, par la pratique. Explication pas à pas.](https://www.youtube.com/watch?v=C0wkeNzQqCg)
- [github repository](https://github.com/ulrich-geraud/kata-tdd-ecommerce-php/blob/main/Readme.fr.md)

### steps

1. **red:**
    1. write the test case.
    1. execute the test case, it fails.
2. **green:**
    1. write the minimum lines of code that makes the test pass.
    1. execute the test case, it passes.
3. **refactor:**
    1. look if there is some improvement to make in the source code or in the tests
    1. execute the tests, they pass.
    1. repeat from step 1 until no improvement are left to do
4. do the same loop with the next test case

### to remember

- the use of TDD allows us to write only meaningful code that we can explain
- use well named parameters to have self-documenting code instead of using comments
- document methods parameters and classes properties using phpdoc for arrays `@param array<string, int|array<string, string|float>> $itemData`
